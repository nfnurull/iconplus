#Daftar pegawai usia 50 tahun atau lebih
SELECT * FROM pegawai WHERE usia >= 50;

#Kantor cabang dengan pegawai terbanyak (Supaya gak butuh relasi cukup id nya aja)
SELECT count(*) AS jumlah, cabang_id FROM pegawai  GROUP BY cabang_id ORDER BY jumlah DESC LIMIT 1;

#Daftar divisi dan jumlah pegawainya
SELECT count(*) AS jumlah, divisi FROM pegawai GROUP BY divisi;